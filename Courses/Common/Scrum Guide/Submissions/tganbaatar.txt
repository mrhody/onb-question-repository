(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web
29 Excel

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Tsetsen erdene Ganbaatar
(Course Site): scrumguides.org
(Course Name): Scrum Guide
(Course URL): http://www.scrumguides.org/docs/scrumguide/v1/Scrum-Guide-US.pdf#zoom=100
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which one of these is NOT a proper Scrum artifact?
(A): Product Backlog
(B): Grooming Backlog
(C): Spring Backlog
(D): User stories
(E): Sprint Velocity
(Correct): B
(Points): 1
(CF): There is no such thing as a Grooming backlog.
(WF): There is no such thing as a Grooming backlog.
(STARTIGNORE)
(Hint):
(Subject): Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Who has final say on what goes into the sprint backlog?
(A): Product Owner
(B): Scrum Master
(C): Development Team
(D): Upper management
(E): Stories are randomly selected for each sprint.
(Correct): C
(Points): 1
(CF): The Sprint Backlog is the Development team's domain.
(WF): The Sprint Backlog is the Development team's domain. Others may debate with the team but the team has final say.
(STARTIGNORE)
(Hint):
(Subject): Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Which one of these NEVER occurs due to a sprint being cancelled?
(A): Some work is reused for other sprints/projects.
(B): Incomplete Stories are re-estimated and put back into the Product Backlog.
(C): Resources are wasted.
(D): The Scrum master is forced to resign.
(E): The event is traumatic to the Scrum Team.
(Correct): D
(Points): 1
(CF): Yes. That would be overkill.
(WF): All except forcing the scrum master to resign are potential effects of a sprint cancellation
(STARTIGNORE)
(Hint):
(Subject): Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 0
(Question): A Sprint Goal is simply the number of story points for a given sprint backlog.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): It's actually an objective of what the software should be able to do by the end of the sprint.
(WF): It's actually an objective of what the software should be able to do by the end of the sprint.
(STARTIGNORE)
(Hint):
(Subject): Scrum
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 0
(Question): Scrum teams are organized by the Scrum Master.
(A):True
(B):False
(Correct): B
(Points): 1
(CF): Scrum teams are self organizing.
(WF): Scrum teams are self organizing.
(STARTIGNORE)
(Hint):
Subject): Scrum
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)