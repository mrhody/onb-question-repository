(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brad Francis
(Course Site): Lynda
(Course Name): Business Analysis Fundamentals
(Course URL): http://www.lynda.com/Business-Business-Skills-tutorials/Business-Analysis-Fundamentals/156546-2.html?srchtrk=index:1%0Alinktypeid:2%0Aq:agile%0Apage:1%0As:relevance%0Asa:true%0Aproducttypeid:2
(Discipline): Business Analysis
(ENDIGNORE)

(Type): true/false
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): It is ideal to use acronyms such as FTE, SR, and NFRs when gathering business requirements. 
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Correct!
(WF): It is best to spell them out fully.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 1
(Question): What are some examples of requirements verification pitfalls?
(A): Limiting validation to just the QA folks
(B): Making verification a one-time event
(C): Analysis paralysis
(D): Not taking a holistic point of view
(Correct): A,B,C,D
(Points): 1
(CF): Correct! 
(WF): All of the answers contribute to the verification pitfalls of requirements.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)

(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): Which is not part of the requirements pyramid?
(A): Individual developers needs
(B): Specifications and design
(C): Your requirements
(D): Project need
(E): Execution
(Correct): A
(Points): 1
(CF): Correct! 
(WF): The requirements pyramid consists of project need, your requirements, specifications and design and execution.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): true/false
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): It is important to clarify boundary conditions while gathering requirements?
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Correct!
(WF): Incorrect.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 1
(Question): What are the types of things a test plan document will cover?
(A): What to test
(B): When to test
(C): Who will test
(D): How to test
(E): Phone numbers of all employees involved
(F): Results of the test
(Correct): A,B,C,D
(Points): 1
(CF): Correct!
(WF): The key components that a test plan will cover are the what, how, when, and who.
(STARTIGNORE)
(Hint): 
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)
