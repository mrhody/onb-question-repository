(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Stephen Brewster
(Course Site): Lynda
(Course Name): Business Analysis Fundamentals
(Course URL): http://www.lynda.com/Business-Business-Skills-tutorials/Business-Analysis-Fundamentals/156546-2.html?srchtrk=index:1%0Alinktypeid:2%0Aq:agile%0Apage:1%0As:relevance%0Asa:true%0Aproducttypeid:2
(Discipline): Business Analysis
(ENDIGNORE)

(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): What is NOT a requirements gathering technique?
(A): Interviewing
(B): Brainstorming
(C): Surveying
(D): Petitioning
(Correct): D
(Points): 1
(CF): Correct!
(WF): See Part 3 - Building the requirements plan
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): true/false
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): The primary activities in the execution phase for a business analyst are change management and product testing. 
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Correct!
(WF): See Part 1 - What is business analysis?
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): true/false
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): A business analyst must be proficient in both business and technical language in order to facilitate between these groups. 
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Correct!
(WF): See Part 1 - Adopting a business analyst mindset
(STARTIGNORE)
(Hint): 
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 1
(Question): Select sources for readily available "as is" information regarding the business.
(A): Company websites
(B): Intranet sites
(C): Business plans
(D): Annual reports
(E): Fox News
(Correct): A,B,C,D
(Points): 1
(CF): Correct! 
(WF): See Part 2 - Capturing business information.
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): true/false
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): Collected business information can be placed into: Organizational Charts, Stakeholder Maps, Context Diagrams, and Use-Case Diagrams.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Correct!
(WF): See Part 2 - Capturing business information
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 1
(Question): When designing your requirements, what are the 4 primary steps?
(A): Understand the scope
(B): Requirements gathering
(C): Requirement refinement
(D): Requirements verification
(E): Manage change
(Correct): A,B,C,D
(Points): 1
(CF): Correct!
(WF): See Part 2 - Designing your requirements
(STARTIGNORE)
(Hint): 
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): true/false
(Category): 15
(Grade style): 0
(Random answers): 1
(Question): It is important to limit the requirements validation to the QA testers only.
(A): True	
(B): False
(Correct): B
(Points): 1
(CF): Correct!
(WF): This is a pitfall! See Part 4 - Verifying your requirements
(STARTIGNORE)
(Hint): 
(Subject): Business Analyst
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 1
(Question): What are the requirement verification techniques described in the videos?
(A): Peer review
(B): Formal inspection
(C): Acceptance criteria
(D): Stakeholder survey
(E): Requirements pyramid
(Correct):  A,B,C
(Points): 1
(CF): Correct!
(WF): See Part 4 - Exploring requirement-verification techniques
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 1
(Random answers): 1
(Question): Select the true statements regarding Training Materials and Procedure Manuals.
(A): A Procedure Manual is intended to be used after training has been completed.
(B): Content should be easy to read.
(C): Text and diagrams should be used with specific examples when possible.
(D): Digital copies should be provided for both.
(E): They are only to be used in one location.
(Correct): A,B,C
(Points): 1
(CF): Correct!
(WF): See Part 4 - Creating procedure manuals and documentation
(STARTIGNORE)
(Hint):
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 15
(Grade style): 0
(Random answers): 0
(Question): After an implementation goes into use, what is NOT a challenge faced by the new users?
(A): The new system looks different and the users get lost.
(B): Users try to make the system do what they 'think' it does.
(C): The work flow decisions are different and they lose trust in the new system.
(D): All of the above.
(Correct): D
(Points): 1
(CF): 
(WF): See Part 5 - Providing implementation support
(STARTIGNORE)
(Hint): 
(Subject): Business Analyst
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

