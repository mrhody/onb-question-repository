(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ziyan Wang
(Course Site): www.lynda.com
(Course Name): SQL Essential Training
(Course URL): http://www.lynda.com/SQL-tutorials/SQL-Essential-Training/139988-2.html
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question):  If CustomerID is a primary key, choose the equivalent answer for
             SELECT COUNT(CustomerID), Country
             FROM Customers
             GROUP BY Country;

(A): SELECT COUNT(*), Country
     FROM Customers
     GROUP BY Country;
(B): SELECT COUNT(CustomerID), Country
     FROM Customers
     ORDER BY Country;
(C): SELECT COUNT(CustomerID), Country
     FROM Customers
     GROUP BY Country ASC;
(D): SELECT COUNT(CustomerID), Country
     FROM Customers
     ORDER BY Country
     GROUP BY Country;
(Correct): A
(Points): 1
(CF): Since CustomerID is a primary key, using an asteroid will also work.
(WF): Since CustomerID is a primary key, using an asteroid will also work.
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Given a table named Customers, what statement below returns all rows that have a value in the column Country that has exactly this pattern I_a_y?
(A): SELECT * FROM Customers WHERE Country LIKE 'I%a%y';
(B): SELECT * FROM Customers WHERE Country LIKE 'I_a_y';
(C): SELECT * FROM Customers WHERE Country LIKE '%I_a_y%';
(D): SELECT * FROM Customers WHERE Country LIKE '_I%a%y_';
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Which of the following is NOT a good SQL practice
(A): Avoid using SELECT * in your query
(B): Always use table aliases when your SQL statement involves more than one source
(C): Utilize SQL subquery
(D): Handle really large operations within one transaction
(Correct): D
(Points): 1
(CF): Handling large operations all in one transaction may cause blocking issues
(WF): Handling large operations all in one transaction may cause blocking issues
(STARTIGNORE)
(Hint):
(Subject): SQL
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)

