(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essentials Training

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Tim Keyser
(Course Site): lynda
(Course Name): SQL Essentials Training
(Course URL): http://www.lynda.com/SQL-tutorials/SQL-Essential-Training/139988-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): What statement is used most for data retrieval?
(A): Select
(B): Insert
(C): Update
(D): Count
(Correct): A
(Points): 1
(CF): The SELECT statement is used for most data retrieval in SQL.
(WF): The SELECT statement is used for most data retrieval in SQL.
(STARTIGNORE)
(Hint):
(Subject): SQL Quick Start
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Select count (*) from table will return the count of the rows in the table.
(A): True
(B): False
(Correct): A 
(Points): 1
(CF): Select count (*) from table will return the count of the rows in the table.
(WF): Select count (*) from table will return the count of the rows in the table.
(STARTIGNORE)
(Hint):
(Subject): SQL Quick Start
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): What are the four basic functions of any database?
(A): Create, retrieve, update and delete
(B): Create, copy, update and remove
(C): Neither of these.
(Correct): A
(Points): 1
(CF): The four basic functions of any complete database system are: create, retrieve, update and delete. 
(WF): The four basic functions of any complete database system are: create, retrieve, update and delete. 
(STARTIGNORE)
(Hint):
(Subject): SQL Quick Start
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): A database is, fundamentally, a collection of tables.
(A): True
(B): False
(Correct): A 
(Points): 1
(CF): A database is, fundamentally, a collection of tables.
(WF): A database is, fundamentally, a collection of tables.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): What year was SQL formalized by the American National Standards Institute?
(A): 1990
(B): 1980 
(C): 1986
(D): 1989
(Correct): C
(Points): 1
(CF): Standard SQL was originally formalized by the American National Standards Institute in 1986. 
(WF): Standard SQL was originally formalized by the American National Standards Institute in 1986. 
(STARTIGNORE)
(Hint):
(Subject): Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): The basic syntax of SQL is complicated.
(A): True
(B): False
(Correct): B 
(Points): 1
(CF): The basic syntax of SQL is very simple.
(WF): The basic syntax of SQL is very simple.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Tables are created using what statement? 
(A): Create
(B): Update
(C): Insert
(D): Select
(Correct): A
(Points): 1
(CF): Tables are created using the create table statement.
(WF): Tables are created using the create table statement.
(STARTIGNORE)
(Hint):
(Subject): Fundamentals
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): Using the join statement, SQL can extract related data from multiple tables.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Relationships
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): In standard SQL, a literal string is represented by a series of characters enclosed in double quote marks.
(A): True
(B): False
(Correct): B 
(Points): 1
(CF): In standard SQL, a literal string is represented by a series of characters enclosed in single quote marks.
(WF): In standard SQL, a literal string is represented by a series of characters enclosed in single quote marks.
(STARTIGNORE)
(Hint):
(Subject): Strings
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 21
(Grade style): 0
(Random answers): 1
(Question): SQL data types are entirely platform dependent.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): SQL data types are entirely platform dependent. The data types that are available on one database management system are almost guaranteed to be different than the data types available on another database management system. 
(WF): SQL data types are entirely platform dependent. The data types that are available on one database management system are almost guaranteed to be different than the data types available on another database management system. 
(STARTIGNORE)
(Hint):
(Subject): Numbers
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)