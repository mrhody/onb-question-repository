(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web
29 Excel

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brandon Bires-Navel
(Course Site): Lynda
(Course Name): Agile Project Management
(Course URL): https://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): How often should the Product Owner work on the product backlog?
(A): All the time, that's there only function
(B): Several hours a week
(C): Every Monday and Friday
(D): Only during the Spring Planning phase
(Correct): B
(Points): 1
(CF): The Product Owner will constantly be clarifying the stories, and adding new ones as they find more work.
(WF): The Product Owner works a few hours a week on Backlog refining to add and clarify stories.
(STARTIGNORE)
(Hint):
(Subject): Backlog Refining
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): The majority of the sprint should be spent planning.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Planning should be done during the Envision phase.
(WF): Planning is important, but if too much time is spent on planning there will be no time to implement the changes.
(STARTIGNORE)
(Hint):
(Subject): Agile phases
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): All of the following are the objectives of Stand-Up meetings except:
(A): Communicating with your teammates
(B): Determine problems that others are being held back by
(C): Stretching out your legs
(D): A 15 minute long meeting that is sharp and to the point
(Correct): C
(Points): 1
(CF): Stand-Up meetings are a great way to communicate with teammates on where they are in the current sprint
(WF): Stretching may be a consequence of Stand-Up meetings, but is not a sole objective
(STARTIGNORE)
(Hint):
(Subject): Stand-Up meetings
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
