(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ziyan Wang
(Course Site): www.lynda.com
(Course Name): Agile Project Management
(Course URL):https://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): Professional
(ENDIGNORE)

(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Agile emphasizes on estimates instead of commitments
(A): True
(B): False
(Correct): A
(Points): 1
(CF): It is true that Agile emphasizes on estimates instead of commitments
(WF): It is true that Agile emphasizes on estimates instead of commitments
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Which statements are true for Sprint and Iteration? (choose three)
(A): Scrum is a specialized incremental development process that uses the term Sprint for its iterations
(B): Sprint is Scrum specific, hence Sprint is an iteration but not all forms of iterations are sprints
(C): Other agile methods may not use the same term Sprint to define Iteration work, but Sprint and Iteration are the two most commonly used terms
(D): An iteration is the same as a sprint, the term usage varies among companies
(E): Each Sprint in a single development cycle must have at least one iteration
(Correct): A,B,C
(Points): 1
(CF): Iteration is the generic agile term, Sprint is Scrum specific
(WF): Iteration is the generic agile term, Sprint is Scrum specific
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What should you NOT do if someone is absent for a meeting frequently?
(A): Talk to the team member privately
(B): Check the team calendar to see if the team member was taking his day off
(C): Ask the team member to give an explanation during the next meeting 
(D): Email the team member
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Agile Project Management
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)

