(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks
29 The Internet & World Wide Web

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Megan Barton
(Course Site): Team Treehouse
(Course Name): How The Internet Works
(Course URL): https://teamtreehouse.com/library/how-the-internet-works/the-internet/the-history-of-the-internet
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 29 
(Grade style): 0
(Random answers): 1
(Question): In the 1960s a government agency called the ______________ gave lots of money to scientists to figure out a way to connect computers around the country.
(A): Federal Bureau of Investigation
(B): National Security Agency
(C): Advance Research Projects Agency
(D): Central Intelligence Agency
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): The Internet
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 1
(Question): In _________ the World Wide Web was invented, riding on the back of the technologies put in place during the expansion of the Internet.
(A): 1891
(B): 1991
(C): 2001
(D): 1901
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): World Wide Web
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 0
(Question): The Internet relies on which of the following hardware and software to work properly:
(A): Servers, routers, switches, computers
(B): Wires, cables, satellites, and dishes
(C): The protocols in the TCP/IP suite
(D): All of the above
(E): None of the above
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): World Wide Web
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 0
(Question): True or False: The developers behind the software used on the Internet agree to follow a set of rules, called protocols, when sending and receiving information.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): TCP/IP
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 0
(Question): True or False: Database servers and personal computers look and function exactly the same way. 
(A): True
(B): False
(Correct): B
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): The Internet
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 1
(Question): Because each packet has to travel through a number of routers to get to its final destination each packet is designed to work _____________. 
(A): With 5 or 6 other packets
(B): With 2 or 3 other packets
(C): With the entire set of packets in a transmission
(D): Independently
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): TCP/IP
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 1
(Question): What determines the best route for a packet to go next on its way to its final destination?
(A): Routers
(B): Switches
(C): Routing Tables
(Correct): C
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): The Internet
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 1
(Question): The primary job of the Internet is to:
(A): launch web browsers
(B): encrypt all messages
(C): make computers more powerful
(D): share data between computers
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): The Internet
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 0
(Question): True or False: The primary networks and core routers around the world make up the backbone of the Internet and support the exchange of Internet traffic between countries, continents and across oceans.
(A): True 
(B): False
(Correct): A
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): The Internet
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)


(Type): multiplechoice
(Category): 29
(Grade style): 0
(Random answers): 1
(Question): Which of the following is not an example of a typical client-server relationship?
(A): When filling out an online profile you choose a picture from a photo sharing site and the photo server transfers the file
(B): Typing the URL of a webpage and a server returning the webpage you requested
(C): After opening your email, Outlook asks for all of your new messages and the mail server provides them to your client
(D): A game server asking about the highest points ever scored in the game and the client returning this information
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): The Internet
(Difficulty): Beginner 
(Applicability): General 
(ENDIGNORE)
