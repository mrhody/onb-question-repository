(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brad Francis
(Course Site): Lynda
(Course Name): Up and Running with Eclipse
(Course URL): http://www.lynda.com/Eclipse-tutorials/Up-Running-Eclipse/111243-2.html
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What is the easiest way to generate getters and setters in Eclipse?
(A): Right click, go to source and select generate getters and setters
(B): Create them by hand
(C): Have a data entry person type them for you
(D): Copy/paste 
(Correct): A
(Points): 1
(CF): The easiest way to generate getters and setters in eclipse is to right click, go to source and select generate getters and setters.
(WF): The easiest way to generate getters and setters in eclipse is to right click, go to source and select generate getters and setters.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What view in eclipse is similar to that of windows explorer?
(A): Package Explorer
(B): Junit View
(C): Navigator 
(D): Outline
(Correct): C
(Points): 1
(CF): The navigator view in eclipse is very similar to windows explorer.
(WF): The navigator view in eclipse is very similar to windows explorer.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): If you want to try and stop at a certain point in your code what steps would you take in eclipse?
(A): Click on the same line in the left gutter to set a breakpoint, simply run as a java application and it will stop.
(B): Eclipse does not allow a user to stop at a breakpoint.
(C): Set a breakpoint by clicking on the left gutter and run as a junit test. 
(D): Click on the same line in the left gutter to set a breakpoint, when you go to run the application select debug as a java application. 
(Correct): D
(Points): 1
(CF): To stop at a breakpoint in eclipse you need to click on the same line in the left gutter to set the breakpoint.  After that you need to execute the code using "debug as" a java application.
(WF): To stop at a breakpoint in eclipse you need to click on the same line in the left gutter to set the breakpoint.  After that you need to execute the code using "debug as" a java application.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 1
(Random answers): 1
(Question): How does one easily resolve missing imports when using classes from a new package? (Select all that apply)
(A): Left click the gutter that show the problem and select the import needed.
(B): Go out on the web to determine the package that needs to be imported and paste it at the top of your class.
(C): Hit ctrl + shift + o to automatically import.
(D): Hover over the item that is marked in red and click the import that is desired.
(Correct): A,C,D
(Points): 1
(CF): There are multiple ways to resolve imports in eclipse here are the options; ctrl + shift + o, left click the gutter where the problem is and select your desired import, or hover over the class that is marked in red and select the desired import.
(WF): There are multiple ways to resolve imports in eclipse here are the options; ctrl + shift + o, left click the gutter where the problem is and select your desired import, or hover over the class that is marked in red and select the desired import.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What is an IDE?
(A): A basic editor.
(B): An environment that facilitates developers in building software.
(C): A utility to connect to databases.
(D): An application that helps with FTPing.
(Correct): B
(Points): 1
(CF): An IDE is an integrated development environment that helps developers build software.
(WF): An IDE is an integrated development environment that helps developers build software.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What is the preferred method of renaming a variable using eclipse?
(A): Search/replace
(B): Manually searching and hand typing the new variable name wherever the old one is found.
(C): Using refactor to rename the variable.
(D): Deleting the class and starting over.
(Correct): C
(Points): 1
(CF): The fast way to rename all instances of a variable within a class in eclipse is to use the refactor > rename function.
(WF): The fast way to rename all instances of a variable within a class in eclipse is to use the refactor > rename function.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What is the term used for a saved view setup within Eclipse?
(A): A perspective.
(B): A view.
(C): The window formatting.
(D): The view Configuration.
(E): The window setup.
(Correct): A
(Points): 1
(CF): The term used for a saved window setup within Eclipse is a perspective.
(WF): The term used for a saved window setup within Eclipse is a perspective.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): The term for "find and replace" in Eclipse is called?
(A): finding.
(B): discovering.
(C): refactoring.
(D): replacing.
(E): searching.
(Correct): C
(Points): 1
(CF): The term for "find and replace" in Eclipse is called refactoring
(WF): The term for "find and replace" in Eclipse is called refactoring
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What is meant when one refers to the gutter in eclipse?
(A): A source of garbage collection
(B): It is where you drag things to be deleted
(C): Any code that doesn't compile goes here
(D): It's the column that icons appear just to the left of the code
(Correct): D
(Points): 1
(CF): The gutter is the column that icons appear in just to the left of the code.
(WF): The gutter is the column that icons appear in just to the left of the code.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What function does "Step Over" perform when debugging?
(A): It skips the function or block of code.
(B): It performs the function or block of code.
(C): It moves line by line through the function or block of code.
(D): Nothing unless it comes across a class.
(E): It runs the rest of the scripts code.
(Correct): C
(Points): 1
(CF): The "Step Over" function walks through the code performing each line one by one as it is repeatedly called.
(WF): The "Step Over" function walks through the code performing each line one by one as it is repeatedly called.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): How can you search your entire workspace for a specific string? 
(A): ctrl + f and type in the string to search.
(B): click search > file and type in the search string.
(C): exit eclipse and use a tool like notepad ++ to search within files
(D): you can't search an entire workspace for text in eclipse.
(Correct): B
(Points): 1
(CF): You can search an entire project by clicking search > file and type in the search string.
(WF): You can search an entire project by clicking search > file and type in the search string.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): If you do not like a view in eclipse how do you get rid of it? 
(A): click the x icon in the top right corner of the view
(B): click and hold the view and drag it into the gutter
(C): double click the view
(D): click and hold the view while dragging to lower right hand corner of the screen.
(Correct): A
(Points): 1
(CF): You can close a view by simply clicking the x icon in the upper right hand corner.
(WF): You can close a view by simply clicking the x icon in the upper right hand corner.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What is an eclipse plugin used for?
(A): A plugin is a way to plug java code into the IDE
(B): An eclipse plugin is a perspective that can be viewed
(C): It is used for extending the functionality of the IDE 
(Correct): C
(Points): 1
(CF): An eclipse plugin is used to extend the functionality of the IDE.
(WF): An eclipse plugin is used to extend the functionality of the IDE.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What is the best way to install an eclipse plugin?
(A): Manually download it and copy it to the workspace directory
(B): Use the eclipse plugin manager to download and install for you
(C): Use the eclipse lazy loader utility to download and install it for you
(Correct): B
(Points): 1
(CF): The eclipse plugin manager is the easiest way to get and install a new plugin.  There are other manual ways to do it as well, but it is not as straight forward.
(WF): The eclipse plugin manager is the easiest way to get and install a new plugin.  There are other manual ways to do it as well, but it is not as straight forward.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): When running in eclipse where do system.out and system.err get output?
(A): They are output to the console view
(B): They get output to a command prompt window 
(C): They go into a special log file that eclipse will create in your workspace
(D): They will be output to the output window in eclipse
(Correct): A
(Points): 1
(CF): The system.out and system.err outputs will get logged to the console view in eclipse.
(WF): The system.out and system.err outputs will get logged to the console view in eclipse.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What version of java does eclipse support?
(A): 1.6
(B): 1.5
(C): 1.5 and up
(D): All versions that you have installed on the machine
(Correct): D
(Points): 1
(CF): Eclipse can support any version of java that you have installed on your working machine.  You simply need to tell eclipse where the installation exists.
(WF): Eclipse can support any version of java that you have installed on your working machine.  You simply need to tell eclipse where the installation exists.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What programming language is supported by eclipse?
(A): Java
(B): C++	
(C): Python
(D): All of the above
(Correct): D
(Points): 1
(CF): Eclipse can support many different programming languages.
(WF): Eclipse can support many different programming languages.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 1
(Random answers): 1
(Question): Which of the choices below are things that you can do with the eclipse debug perspective? (Select all that apply)
(A): Set breakpoints 
(B): Watch variables
(C): View objects and their data
(D): Step through the code being executed
(Correct): A,B,C,D
(Points): 1
(CF): You can do many things with the eclipse debugger such as; setting breakpoints, watching variables, viewing objects, and stepping through the code.
(WF): You can do many things with the eclipse debugger such as; setting breakpoints, watching variables, viewing objects, and stepping through the code.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): How do you add vm args to your application when running with eclipse?
(A): You are not able to set vm args
(B): You can enter them in the console
(C): Go to run configurations, arguments tab, and enter in the vm arguments section
(Correct): C
(Points): 1
(CF): Go to run configurations, arguments tab, and enter in the vm arguments section
(WF): Go to run configurations, arguments tab, and enter in the vm arguments section
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): How do you add program arguments when running your application in eclipse?
(A): Go to run configurations, arguments tab, and enter in the program arguments section
(B): You can enter them in the console
(C): Go to the debugging view, select the arguments tab and enter them there
(Correct): A
(Points): 1
(CF): Go to run configurations, arguments tab, and enter in the program arguments section.
(WF): Go to run configurations, arguments tab, and enter in the program arguments section.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)
