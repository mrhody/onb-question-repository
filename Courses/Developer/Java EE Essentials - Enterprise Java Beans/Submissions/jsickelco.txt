(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Jeff Sickelco
(Course Site): www.lynda.com
(Course Name): Java EE Essentials: Enterprise Java Beans
(Course URL) : http://www.lynda.com/Java-tutorials/Java-EE-Essentials-Enterprise-JavaBeans/170059-2.html
(Discipline): Technical 
(ENDIGNORE)

(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What attribute indicates that a field will function as an Entity Bean's primary key?
(A): @ID
(B): @PrimaryKey
(C): @BeanGUID
(D): @SessionID
(E): @Entity
(Correct): A
(Points): 1
(CF): The @ID attribute indicates a primary key.
(WF): The @ID attribute indicates a primary key.
(STARTIGNORE)
(Hint):
(Subject): Enterprise Java Beans
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1 
(Grade style): 0
(Random answers): 1
(Question): Which of the following is NOT functionality that is usually associated with a client-side application?
(A): Communicating with a server
(B): Accepting user input
(C): Displaying data for users
(D): Accessing local storage, such as cookies
(E): Performing authentication
(Correct): E
(Points): 1
(CF): Authentication is generally associated with server-side functionality.
(WF): Authentication is generally associated with server-side functionality.
(STARTIGNORE)
(Hint):
(Subject): Enterprise Java Beans
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What are the two types of interfaces needed by Java beans?
(A): The "Home" and "Component" interfaces
(B): The "Active" and "Passive" interfaces
(C): The "State" and "Message" interfaces
(D): The "Entity" and "Session" interfaces
(E): The "Client" and "Server" interfaces
(Correct): A
(Points): 1
(CF): All beans require a "Home" and "Component" interface.
(WF): All beans require a "Home" and "Component" interface.
(STARTIGNORE)
(Hint):
(Subject): Enterprise Java Beans
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What is Amdahl's law?
(A): With enough processors, the time it takes to run a program is equal to the time needed to run its slowest non-parallelizable section
(B): The number of transistors on a given area of a computer chip doubles every 18 months
(C): The speed of light puts a practical limit on the efficiency of any computer processor
(D): "Concurrent read, concurrent write" access control poses a danger to any software operation of non-trivial complexity
(E): The latency of a given system grows by a factor of two for each remote server added to its network
(Correct): A
(Points): 1
(CF): Amdahl's law states that with enough processors, the time it takes to run a program is equal to the time needed to run its slowest non-parallelizable section.
(WF): Amdahl's law states that with enough processors, the time it takes to run a program is equal to the time needed to run its slowest non-parallelizable section.
(STARTIGNORE)
(Hint):
(Subject): Enterprise Java Beans
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What is asymptotic complexity?
(A): A measurement of the amount of time taken by an algorithm that scales with input size
(B): A measurement of the amount of time taken by the portions of an algorithm that remain constant regardless of input size
(C): A measurement of how deeply nested a given piece of code's instructions are
(D): A measure of latency in a distributed system
(E): The amount of storage space needed by an algorithm relative to its input size
(Correct): A
(Points): 1
(CF): Asymptotic complexity is a measure of the amount of time taken by an algorithm that scales with input size.
(WF): Asymptotic complexity is a measure of the amount of time taken by an algorithm that scales with input size.
(STARTIGNORE)
(Hint):
(Subject): Enterprise Java Beans
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which attribute would be used to indicate that a method can only be accessed by callers who belong to the "admin" role?
(A): @Protect(role="admin")
(B): @Asynchronous
(C): @Authorize("admin")
(D): @Deny(!admin)
(E): @RolesAllowed(value={"admin"})
(Correct): E
(Points): 1
(CF): You can guard against unauthorized method access by using the @RolesAllowed attribute and setting the value to the role name.
(WF): You can guard against unauthorized method access by using the @RolesAllowed attribute and setting the value to the role name.
(STARTIGNORE)
(Hint):
(Subject): Enterprise Java Beans
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What type of bean is NOT associated with a single client session?
(A): Session Bean
(B): Stateful Bean
(C): Stateless Bean
(D): Entity Bean
(E): Singleton Bean
(Correct): D
(Points): 1
(CF): An Entity Bean exists across sessions. Stateful, stateless, and singleton beans are sub-categories of Session Beans.
(WF): An Entity Bean exists across sessions. Stateful, stateless, and singleton beans are sub-categories of Session Beans.
(STARTIGNORE)
(Hint):
(Subject): Enterprise Java Beans
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which of the following transaction modes will always force the creation of a new transaction, even if a current transaction exists?
(A): Required
(B): RequiresNew
(C): Supported
(D): ForceNew
(E): CreateNew
(Correct): B
(Points): 1
(CF): The RequiresNew mode will force the creation of a new transaction. Required will use the current transaction if it exists, and Supported will not create a transaction. The other choices are not valid modes.
(WF): The RequiresNew mode will force the creation of a new transaction. Required will use the current transaction if it exists, and Supported will not create a transaction. The other choices are not valid modes.
(STARTIGNORE)
(Hint):
(Subject): Enterprise Java Beans
(Difficulty): Advanced
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which of the following is NOT a recommended strategy for maximizing efficiency?
(A): Give equal work to each processor
(B): Identify sections of code that can be parallelized
(C): Minimize communication overhead
(D): Always use CRCW access control
(E): Use built-in Java functions
(Correct): D
(Points): 1
(CF): CRCW access control may not be safe to use in all instances; evaluate access control on a case-by-case basis.
(WF): CRCW access control may not be safe to use in all instances; evaluate access control on a case-by-case basis.
(STARTIGNORE)
(Hint):
(Subject): Enterprise Java Beans
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which of the following is required of a Java Bean? 
(A): It needs to extend the Bean class
(B): It needs to implement the Component interface
(C): It needs to have a public constructor with no arguments
(D): It needs a Destroy method
(E): It must handle its own garbage collection
(Correct): C
(Points): 1
(CF): Java beans need to have a public constructor with no arguments.
(WF): Java beans need to have a public constructor with no arguments.
(STARTIGNORE)
(Hint):
(Subject): Enterprise Java Beans
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)