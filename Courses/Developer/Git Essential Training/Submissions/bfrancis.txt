(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Brad Francis
(Course Site): Lynda
(Course Name): Git Essential Training
(Course URL): http://www.lynda.com/Git-tutorials/Git-Essential-Training/100222-2.html?srchtrk=index:1%0Alinktypeid:2%0Aq:GIT%0Apage:1%0As:relevance%0Asa:true%0Aproducttypeid:2
(Discipline): Technical
(ENDIGNORE)

(Type): truefalse
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): The GIT architecture tracks both files and directories.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): GIT is a file based version control system.   
(WF): GIT is a file based version control system. 
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): It is not all that important to add rich/relevant messages with a commit.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): The commit messages are crucial for other users to see what was going on with your commit.  
(WF): The commit messages are crucial for other users to see what was going on with your commit.  
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): How does one create an empty directory in GIT?
(A): They can't
(B): Simply add the directory and commit
(C): Create a .gitignore file
(Correct): C
(Points): 1
(CF): Git tracks files and you can't have empty directories, the most common practice is to add a .gitignore file.
(WF): Git tracks files and you can't have empty directories, the most common practice is to add a .gitignore file.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): What are the underlying steps of a git pull command?
(A): git status, followed by git fetch
(B): git fetch, followed by git merge
(C): git merge, followed by git fetch
(D): there are no underlying steps to git pull 
(Correct): B
(Points): 1
(CF): git pull is the same thing as doing get fetch and then a git merge all wrapped into one command.
(WF): git pull is the same thing as doing get fetch and then a git merge all wrapped into one command.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): It is a good habit to always fetch/merge(pull) before pushing to the remote repository?
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Always pull before you push to avoid errors and issues with git.   
(WF): Always pull before you push to avoid errors and issues with git.   
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 13
(Grade style): 0
(Random answers): 1
(Question): What are some of the useful things that you can do with git log?
(A): Search for commits based on a user
(B): Search for commits between timeframes
(C): Search for specific commits on a branch
(Correct): A, B, C
(Points): 1
(CF): git log is very powerful and helpful when trying to search for specific commits.
(WF): git log is very powerful and helpful when trying to search for specific commits.
(STARTIGNORE)
(Hint):
(Subject): Git Essential Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
