(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Stephen Cerbone
(Course Site): Lynda.com
(Course Name): Java Essential Training
(Course URL): http://www.lynda.com/Java-tutorials/Essential-Training/86005-2.html
(Discipline): Technical
(ENDIGNORE)

(Type): truefalse
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Java is a case sensitive language.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Java is a case sensitive language.
(WF): Java is a case sensitive language.
(STARTIGNORE)
(Hint):
(Subject): Java Essential Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What does every Java program have?
(A): A package declaration.
(B): A class declaration.
(C): A main method.
(D): An abstract class.
(E): A super class.
(Correct): A, B, C
(Points): 1
(CF): Every Java program has a package declaration, a class declaration, and a main method.
(WF): Every Java program has a package declaration, a class declaration, and a main method.
(STARTIGNORE)
(Hint):
(Subject): Java Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which type of variable should you use if you need precision?
(A): Double
(B): Float
(C): Integer
(D): Long
(E): BigDecimal
(Correct): E
(Points): 1
(CF): A BigDecimal variable is much more precise than Java's primitive variables.
(WF): A BigDecimal variable is much more precise than Java's primitive variables.
(STARTIGNORE)
(Hint):
(Subject): Java Essential Training
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Encapsulation is the process of creating documentation for key components of a program.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Encapsulation is splitting code up into individual classes and functions.
(WF): Encapsulation is splitting code up into individual classes and functions.
(STARTIGNORE)
(Hint):
(Subject): Java Essential Training
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): A instance variable is
(A): any variable within a method.
(B): public variables within a method.
(C): any variable within a class.
(D): any variable within a class and outside of all methods.
(E): public variables within a class and outside of all methods.
(Correct): D
(Points): 1
(CF): An instance variable is any variable within a class and outside of all methods.
(WF): An instance variable is any variable within a class and outside of all methods.
(STARTIGNORE)
(Hint):
(Subject): Java Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Which of the following is a syntactically correct constructor? (Class name is: MyClass)
(A): public static MyClass(){}
(B): public static void MyClass(){}
(C): public MyClass(){}
(D): private static void MyClass(){}
(E): private void MyClass(){}
(Correct): C
(Points): 1
(CF): "public MyClass(){}" is a syntactically correct constructor.
(WF): "public MyClass(){}" is a syntactically correct constructor.
(STARTIGNORE)
(Hint):
(Subject): Java Essential Training
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): truefalse
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Getters and setters are implicit functions that read and write to a private variable in a class.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Getters and setters are explicit functions that read and write to a private variable in a class.
(WF): Getters and setters are explicit functions that read and write to a private variable in a class.
(STARTIGNORE)
(Hint):
(Subject): Java Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): Constants in Java are uppercase because of convention.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Constants in Java are uppercase because of convention.
(WF): Constants in Java are uppercase because of convention.
(STARTIGNORE)
(Hint):
(Subject): Java Essential Training
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): What is true about overloading a constructor in Java?
(A): You can never do it.
(B): You can only do it once.
(C): You can only do it with the same parameters.
(D): You can only do it with different parameters.
(E): You can only do it if the constructor is public and void.
(Correct): D
(Points): 1
(CF): You can overload a constructor if you have a different number of parameters or a different type.
(WF): You can overload a constructor if you have a different number of parameters or a different type.
(STARTIGNORE)
(Hint):
(Subject): Java Essential Training
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)


(Type): truefalse
(Category): 1
(Grade style): 0
(Random answers): 1
(Question): When a method is protected it can still be called from any of its subclasses.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): When a method is protected it can still be called from any of its subclasses.
(WF): When a method is protected it can still be called from any of its subclasses.
(STARTIGNORE)
(Hint):
(Subject): Java Essential Training
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)