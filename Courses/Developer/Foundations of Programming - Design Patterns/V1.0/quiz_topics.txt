List of principles
==================

* Code to an interface
* Don't repeat yourself
* Encapsulate what varies
* Favor loose coupling
* Favor low coupling and high cohesion
* Open/Closed Principle
* Single Responsibility Principle
* Liskov Substitution Principle
* Interface Segregation Principle
* Dependency Injection Principle


Pattern definitions
===================

Abs Factory:
Provide an interface for creating families of related or dependent objects without specifying their concrete classes

Builder:
Separate the construction of a complex object from its representation so that the same construction process can create different representations

Factory Method:
Defines an interface for creating an object, but lets subclasses decide which class to instantiate

Singleton:
Ensure a class has only one instance, and provide a global point of access to it

Adapter:
Convert the interface of a class into another interface clients expect

Composite:
Compose objects into tree structures to represent whole-part hierarchies

Decorator:
Attach additional responsibilities to an object dynamically; providing a flexible alternative to subclassing

Facade:
Provide a unified interface to a set of interfaces in a subsystem

Proxy:
Provide a surrogate or placeholder for another object to control access to it

Strategy:
Defines a family of algorithms, encapsulates each one, and makes them interchangeable

State:
Allow an object to alter its behavior when its internal state changes

Observer:
Defines one to many dependencies between objects so that when one object changes state, all of its dependents are notified and updated automatically

Iterator:
Provides a way to access the elements of an aggregate object sequentially without exposing its underlying representation

Command:
Encapsulate a request as an object, thereby letting you parameterize clients with different requests

Mediator:
Define an object that encapsulates how a set of objects interact

Template Method:
Define the skeleton of an algorithm in an operation, deferring some steps to client subclasses

Visitor:
Represent an operation to be performed on the elements of an object structure
