(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
25 RESTful API Testing with Postman
26 Experience Design Patterns in Java
27 Java Web Services
28 Windows Tips & Tricks

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Kamalesh Patil
(Course Site): udemy.com
(Course Name): Java Web Services
(Course URL): https://www.udemy.com/java-web-services/learn/v4/overview
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 27
(Grade style): 0
(Random answers): 1
(Question): __________ is a set of principles and __________ is a way to implement them.
(A): SOAP, JAX-WS
(B): SOA, JAX-WS
(C): SOA, Web Services
(D): REST, JAX-WS
(Correct): C
(Points): 1
(CF): SOA (Service Oriented Architecture) is a set of principles which can be implemented using Web Services.
(WF): SOA (Service Oriented Architecture) is a set of principles which can be implemented using Web Services.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 27
(Grade style): 0
(Random answers): 0
(Question): True or False: A well-formed XML document implies that it is a valid XML document.
(A): TRUE
(B): FALSE
(Correct): B
(Points): 1
(CF): A valid XML document is well formed and it conforms to a specified XML Schema. An XML document may be well formed but it may not conform to a schema.
(WF): A valid XML document is well formed and it conforms to a specified XML Schema. An XML document may be well formed but it may not conform to a schema.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 27
(Grade style): 0
(Random answers): 0
(Question): What is the relationship between HTML and XML?
(A): HTML is used in RESTful Web Services, XML is used in SOAP Web Services
(B): HTML is a subset of XML
(C): HTML is used on World Wide Web, XML is used on Internet
(D): Other than the fact they are both Markup Languages, they are unrelated. 
(Correct): D
(Points): 1
(CF):
(WF):
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 27
(Grade style): 0
(Random answers): 1
(Question): What are the two implementation approaches for SOAP Web Services?
(A): WSDL, WADL
(B): Apache CXF, Jersey
(C): JAX-WS, JAX-RS
(D): Top Down, Bottom UP
(Correct): D
(Points): 1
(CF): The two implementation approaches for SOAP Web Services are - Top Down (Contract First) and Bottom First (Code First).
(WF): The two implementation approaches for SOAP Web Services are - Top Down (Contract First) and Bottom First (Code First).
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 27
(Grade style): 0
(Random answers): 1
(Question): What are the two portions of a WSDL file?
(A): Abstract and Physical
(B): Types and Operations
(C): Bindings and Services
(D): Data and Transport
(Correct): A
(Points): 1
(CF): A WSDL file is divided in two portions.  The Abstract portion describes which services are provided and the Physical portion describes how the services are provided.
(WF): A WSDL file is divided in two portions.  The Abstract portion describes which services are provided and the Physical portion describes how the services are provided.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 27
(Grade style): 0
(Random answers): 1
(Question): Which of the following JAXB tools generates Java classes from an XML Schema?
(A): schemagen
(B): xjc
(C): java2wsdl
(D): wsdl2java
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 27
(Grade style): 0
(Random answers): 1
(Question): When your application has a lot of non-functional requirements (NFRs) then - 
(A): you should not use Web Services
(B): you should develop a Web Application instead of a Web Service
(C): you should prefer SOAP Web Services
(D): you should prefer RESTful Web Services
(Correct): C
(Points): 1
(CF): You should prefer SOAP Web Services when you have a lot of NFRs because SOAP has support for many standards for NFRs, and many SOAP WS Engines/Stacks support these standards.
(WF): You should prefer SOAP Web Services when you have a lot of NFRs because SOAP has support for many standards for NFRs, and many SOAP WS Engines/Stacks support these standards.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 27
(Grade style): 0
(Random answers): 1
(Question): The key advantages of RESTful Web Services over SOAP Web Services are?  Please select three.
(A): RESTful is a newer standard
(B): Statelessness
(C): Loose Coupling
(D): Support for multiple data formats
(E): Less overhead
(Correct): B,D,E
(Points): 1
(CF): RESTful architecture emphasizes stateless APIs which enable scalability.  RESTful APIs can support multiple data formats whereas SOAP only works with XML.  SOAP WS have considerable overhead compared to RESTful WS.
(WF): RESTful architecture emphasizes stateless APIs which enable scalability.  RESTful APIs can support multiple data formats whereas SOAP only works with XML.  SOAP WS have considerable overhead compared to RESTful WS.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 27
(Grade style): 0
(Random answers): 1
(Question): Which HTTP Method should be used to create a resource?
(A): PUT
(B): UPDATE
(C): SUBMIT
(D): POST
(Correct): D
(Points): 1
(CF):
(WF):
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 27
(Grade style): 0
(Random answers): 1
(Question): In RESTful architecture, URIs represent - 
(A): States
(B): Operations
(C): Resources
(D): Nouns
(Correct): C
(Points): 1
(CF):
(WF):
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
