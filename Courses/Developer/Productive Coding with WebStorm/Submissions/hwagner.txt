(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Hayden Wagner
(Course Site): Udemy
(Course Name): Productive Coding with WebStorm
(Course URL): https://www.udemy.com/productive-coding-with-webstorm/learn/v4/overview
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 0 
(Grade style): 0
(Random answers): 1
(Question): A/An _____ is solely focused on coding and usually requires other tools for development. A/An _____ includes tools for coding, refactoring, building, testing, version control, debugging etc.
(A): code scrambler, integrated development environment (IDE)
(B): editor, integrated development environment (IDE)
(C): text editor, improved development instrument (IDI)
(D): integrated development environment (IDE), editor
(E): coder, developer
(Correct): B
(Points): 1
(CF): Default editor programs are focused only on writing code, while integrated development environments (IDEs) include many development functions in a contained program.
(WF): Default editor programs are focused only on writing code, while integrated development environments (IDEs) include many development functions in a contained program.
(STARTIGNORE)
(Hint):
(Subject): Introduction to WebStorm
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What is the default shortcut key to go back to the editor window immediately in WebStorm?
(A): Alt + Backspace
(B): Ctrl + Alt + Delete
(C): Tab + e
(D): Escape 
(Correct): D
(Points): 1
(CF): 
(WF): 
(STARTIGNORE)
(Hint):
(Subject): Coding Faster with WebStorm
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What WebStorm feature is used to quickly find and change settings or execute actions? 
(A): Find action
(B): Quick finder
(C): Search and use
(D): Preference dialog 
(Correct): A
(Points): 1
(CF): The “find action” feature allows users to change the way their IDE looks and behaves in a fast and effective way. The preference dialog allows you to change IDE settings and behavior, but it is not the quickest option in most cases.
(WF): The “find action” feature allows users to change the way their IDE looks and behaves in a fast and effective way. The preference dialog allows you to change IDE settings and behavior, but it is not the quickest option in most cases.
(STARTIGNORE)
(Hint):
(Subject): Coding Faster with WebStorm 
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): It is not possible to have to multiple keyboard shortcuts key mapped to the same action in WebStorm.
(A): True
(B): False
(Correct): B 
(Points): 1
(CF): It is possible to map multiple keyboard commands to single WebStorm actions. This can be useful for mapping a custom shortcut to an action while keeping the default WebStorm mapping intact.
(WF): It is possible to map multiple keyboard commands to single WebStorm actions. This can be useful for mapping a custom shortcut to an action while keeping the default WebStorm mapping intact.
(STARTIGNORE)
(Hint):
(Subject): Coding Faster with WebStorm
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What is the special variable used to place the cursor when writing a new live template for WebStorm?
(A): [code]$FINAL$[/code]
(B): [code]$CURSOR$[/code]
(C): [code]$END$[/code]
(D): [code]/*END*/[/code]
(E): [code]__CURSOR__[/code]
(Correct): C
(Points): 1
(CF): “END” wrapped in dollar signs ($) is the special variable used in live templates to place the cursor (capitalization is not necessary but it is best practice).
(WF): “END” wrapped in dollar signs ($) is the special variable used in live templates to place the cursor (capitalization is not necessary but it is best practice).
(STARTIGNORE)
(Hint):
(Subject): Live Templates
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): _____ templates are snippets of code that get inserted into a file, and ______ templates are templates used to generate complete starting structures for files.
(A): Code, page
(B): File, live
(C): Code, complete
(D): Live, file
(Correct): D 
(Points): 1
(CF): Live templates are code snippets that can be quickly inserted into a file. File templates are used to add a starting base structure to certain file types.
(WF): Live templates are code snippets that can be quickly inserted into a file. File templates are used to add a starting base structure to certain file types.
(STARTIGNORE)
(Hint):
(Subject): Templates
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Placed breakpoints and bookmarks show up in the “Structure” tool window.
(A): True
(B): False
(Correct): B 
(Points): 1
(CF): Breakpoints and bookmarks show up in the “Favorites” tool window.
(WF): Breakpoints and bookmarks show up in the “Favorites” tool window.
(STARTIGNORE)
(Hint):
(Subject): WebStorm Tips and Trips
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What does the “Put label” option in the WebStorm “Local history” feature do?
(A): Adds the label to the local history timeline for the all project files
(B): Adds a new Git tag to the project’s Git repo
(C): Adds a visual label to the editor tab for the corresponding file
(D): Adds the label to the local history timeline for only the selected file at the time of label creation
(Correct): A
(Points): 1
(CF): The “Put label” option adds a point in the WebStorm local history that is visible in the local history of all project files. This does not show up on the editor tab and it does not affect the project’s Git repo or history.
(WF): The “Put label” option adds a point in the WebStorm local history that is visible in the local history of all project files. This does not show up on the editor tab and it does not affect the project’s Git repo or history.
(STARTIGNORE)
(Hint):
(Subject): Working with Local History
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What tool can be used to test a POST request to a web service within WebStorm?
(A): Postman
(B): the “Test a RESTful Web Service” feature (“REST Client” tool)
(C): RESTtester
(D): testmyrest.com plugin
(Correct): B
(Points): 1
(CF): The “Test a RESTful Web Service” in WebStorm allows users to send and view requests and responses from a REST API. 
(WF): The “Test a RESTful Web Service” in WebStorm allows users to send and view requests and responses from a REST API. 
(STARTIGNORE)
(Hint):
(Subject): Testing and Debugging Web Services
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): JetBrains allows third party plugins to be used in WebStorm.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Third party plugins are allowed and can be installed through the Plugins settings dialog. 
(WF): Third party plugins are allowed and can be installed through the Plugins settings dialog. 
(STARTIGNORE)
(Hint):
(Subject): WebStorm Plugins
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)
