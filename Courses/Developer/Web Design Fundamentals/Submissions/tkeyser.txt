(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Tim Keyser
(Course Site): Lynda
(Course Name): Web Design Fundamentals
(Course URL): 
(Discipline): Professional
(ENDIGNORE)

(Type): truefalse
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): HTML is at the heart of every web page.
(A): True
(B): False
(Correct): A 
(Points): 1
(CF): HTML is at the heart of every web page.
(WF): HTML is at the heart of every web page.
(STARTIGNORE)
(Hint):
(Subject): Exploring Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): You want to make sure you are not using a plain text document when coding.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): You want to make sure you are using an editor with plain text for coding, not rich text.
(WF): You want to make sure you are using an editor with plain text for coding, not rich text.
(STARTIGNORE)
(Hint):
(Subject): Exploring Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What does CSS stand for?
(A): Crammed Style Sheets
(B): Creative Style Sheets
(C): Cascading Style Sheets
(D): Creative Style Scenes
(Correct): C
(Points): 1
(CF): CSS stands for Cascading Style Sheets, which are used to control the visual presentation of web pages.
(WF): CSS stands for Cascading Style Sheets, which are used to control the visual presentation of web pages.
(STARTIGNORE)
(Hint):
(Subject): Exploring Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What does CMS stand for?
(A): Content Management System
(B): Conventional Markup System
(C): Conversational Management System
(D): Cascading Management System
(Correct): A
(Points): 1
(CF): CMS stands for Content Management System and typically stores content in some type of database.
(WF): CMS stands for Content Management System and typically stores content in some type of database.
(STARTIGNORE)
(Hint):
(Subject): Getting started
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): One of the most important decisions a new web designer can make is choosing which suite of programs to use for creating and building websites.
(A): True
(B): False
(Correct): A 
(Points): 1
(CF): One of the most important decisions a new web designer can make is choosing which suite of programs to use for creating and building websites.
(WF): One of the most important decisions a new web designer can make is choosing which suite of programs to use for creating and building websites.
(STARTIGNORE)
(Hint):
(Subject): Exploring Tools
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): W3C and ECMAScript are two good sites to reference regarding good standards for HTML and CSS. 
(A): True
(B): False
(Correct): A 
(Points): 1
(CF): W3C and ECMAScript are two good sites to reference regarding good standards for HTML and CSS. 
(WF): W3C and ECMAScript are two good sites to reference regarding good standards for HTML and CSS. 
(STARTIGNORE)
(Hint):
(Subject): Learning Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): The biggest recent shift in web design has been the increased focus on well commented code.
(A): True
(B): False
(Correct): B 
(Points): 1
(CF): One of the biggest recent shifts in web design has been the increased focus on content.
(WF): One of the biggest recent shifts in web design has been the increased focus on content.
(STARTIGNORE)
(Hint):
(Subject): Learning Web Design
(Difficulty): Beginner 
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What has been the biggest change in web design?
(A): Screen resolution 
(B): Mobile devices
(C): Large TVs
(D): Powerful desktops
(Correct): B
(Points): 1
(CF): The biggest change in web design has been the rise of mobile devices.
(WF): The biggest change in web design has been the rise of mobile devices.
(STARTIGNORE)
(Hint):
(Subject): Learning Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): What four segments are most workflows broken down into?
(A): Brainstorm, discussion, development, production
(B): Planning, design, development, and publishing
(C): Design, development, test, publishing
(Correct): B
(Points): 1
(CF): Planning, design, development, and publishing
(WF): Planning, design, development, and publishing
(STARTIGNORE)
(Hint):
(Subject): Learning Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 22
(Grade style): 0
(Random answers): 1
(Question): Javascript complements the static nature of HTML by making it possible to create interactive elements, display elements based on conditions, animate items, and more.
(A): True
(B): False
(Correct): A 
(Points): 1
(CF): Javascript complements the static nature of HTML by making it possible to create interactive elements, display elements based on conditions, animate items, and more.
(WF): Javascript complements the static nature of HTML by making it possible to create interactive elements, display elements based on conditions, animate items, and more.
(STARTIGNORE)
(Hint):
(Subject): Learning Web Design
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)