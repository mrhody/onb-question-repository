(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals
23 Programming Foundations: Fundamentals
24 Java SE 8 Bootcamp
26 Experience Design Patterns in Java

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Kamalesh Patil
(Course Site): udemoy.com
(Course Name): Experience Design Patterns in Java
(Course URL): https://www.udemy.com/experience-design-patterns/learn/v4/overview
(Discipline): Technical
(ENDIGNORE)


(Type): multiplechoice
(Category): 26
(Grade style): 0
(Random answers): 1
(Question): Which of the following statements is true?
(A): A Design Pattern is a framework for developing Enterprise Applications.
(B): A Design Pattern is a framework for developing Java Applications.
(C): A Design Pattern describes a recurring problem and an implementation of the problem using Enterprise Application Frameworks.
(D): A Design Pattern describes a recurring problem and the core of the solution to that problem.
(Correct): D
(Points): 1
(CF): A Design Pattern describes a recurring problem in a software system and an integral part of solution to the problem.
(WF): A Design Pattern describes a recurring problem in a software system and an integral part of solution to the problem.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 26
(Grade style): 0
(Random answers): 1
(Question): Which of the following are main categories of Design Patterns?
(A): Structural
(B): Transitional
(C): Creational
(D): Temporal
(Correct): A,C
(Points): 1
(CF): The three main categories of Design Patterns are Creational, Structural and Behavioral.
(WF): The three main categories of Design Patterns are Creational, Structural and Behavioral.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 26
(Grade style): 0
(Random answers): 1
(Question): The BufferedWriter class from Java IO API is an example of this Design Pattern?
(A): Delegation
(B): Proxy
(C): Decorator
(D): Mediator
(Correct): C
(Points): 1
(CF): The BufferedWriter class wraps a Writer instance and adds additional capability to it, namely, buffering.  It is an example of the Decorator Pattern.
(WF): The BufferedWriter class wraps a Writer instance and adds additional capability to it, namely, buffering.  It is an example of the Decorator Pattern.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 26
(Grade style): 0
(Random answers): 0
(Question): True or False: The Prototype Design Pattern belongs to the Inversion of Control Design Pattern Category.
(A): TRUE
(B): FALSE
(Correct): B
(Points): 1
(CF): Inversion of Control is just another Design Pattern and not a Category.
(WF): Inversion of Control is just another Design Pattern and not a Category.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 26
(Grade style): 0
(Random answers): 1
(Question): __________ Design Pattern reduces namespace by avoiding creation of global variables.
(A): Prototype
(B): Singleton
(C): Proxy
(D): Command
(Correct): C
(Points): 1
(CF):
(WF):
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 26
(Grade style): 0
(Random answers): 1
(Question): __________ promotes code reuse and __________ promotes loose coupling.
(A): Immutability, Delegation
(B): Inheritance, Refactoring
(C): Programming to Interface, Immutability
(D): Delegation, Programming to Interface
(Correct): D
(Points): 1
(CF): One of the key benefits of using Delegation is code reuse; Programming to Interface promotes loose coupling between components.
(WF): One of the key benefits of using Delegation is code reuse; Programming to Interface promotes loose coupling between components.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 26
(Grade style): 0
(Random answers): 1
(Question): This Design Pattern is good for representing part-whole hierarchies of objects.
(A): Bridge
(B): Composite
(C): Mediator
(D): Visitor
(Correct): B
(Points): 1
(CF):
(WF):
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 26
(Grade style): 0
(Random answers): 1
(Question): You are developing Search Products functionality for a retail website where the user can
request Products be listed based on a criteria such as cheapest first, highest rated first, best selling first etc.
Which Design Pattern is best suited to handle displaying Product Listings based on the user preference?
(A): Command
(B): Strategy
(C): Decorator
(D): Adapter
(Correct): B
(Points): 1
(CF):
(WF):
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 26
(Grade style): 0
(Random answers): 1
(Question): Movie Theater chain "A" acquired another chain "B".  Company A wants to integrate Booking Service of
Company B into their Booking System - but their interfaces are drastically different.
What Design Pattern is best suited to allow the two systems to work together?
(A): Decorator
(B): Strategy
(C): Facade
(D): Adapter
(Correct): D
(Points): 1
(CF): Adapter Design Pattern allows components work together that couldn't otherwise because of incompatible interfaces.
(WF): Adapter Design Pattern allows components work together that couldn't otherwise because of incompatible interfaces.
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 26
(Grade style): 0
(Random answers): 1
(Question): __________ Design Pattern is used to subscribe to Object state changes.
(A): Listener
(B): Observer
(C): Adapter
(D): State
(Correct): B
(Points): 1
(CF):
(WF):
(STARTIGNORE)
(Hint):
(Subject):
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)
