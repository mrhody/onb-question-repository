==========
Objective: Given a scenario, select an appropriate pattern.
==========

[[
You are building business software that performs financial projections.
There are several algorithms that can perform the year-end sales projections.
You need to allow the user to select a specific algorithm at run-time.
<br/><br/>
Which pattern would you apply to solve this design problem?
]]
1: State
*2: Strategy
3: Decorator
4: Template Method


[[
You are designing a UI wizard that has a complex workflow with multiple
paths through the wizard's dialogs.  The paths are event-driven and completely
deterministic.
<br/><br/>
Which pattern would you apply to solve the workflow aspect of this design problem?
]]
*1: State
2: Proxy
3: Composite
4: Factory Method

